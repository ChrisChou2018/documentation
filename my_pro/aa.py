"""aa模块的文档注释"""


class Aa(object):
    """
        Aa类的注释
    """
 
    @staticmethod
    def aa_api(x, y):
        """
        求商
        
        :param x: 整数
        :param y: 不能为零的整数
        :return: 两数之商
        """
        return x/y
    
