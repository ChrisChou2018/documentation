sphinx学习笔记
==============

*初步探索学习sphinx*

环境搭建
********

首先使用python非常便捷的pip工具安装sphinx

``pip install sphinx``

安装完成后重新打开terminal环境，此事sphinx的几个常用的基本命令就可以使用了，分别是
sphinx-quickstart
sphinx-build

首先创建一个目录后cd进入执行命令

``sphinx-quickstart``

在经过几个基本的选项后就会生成一个基本的项目目录
如果你的项目选项选择的是source和build目录分开的话，目录结构会是这样
::

    ├── Makefile
    ├── build
    ├── make.bat
    └── source
        ├── _static
        ├── _templates
        ├── conf.py
        ├── index.rst

我的使用
********

source目录是你的文档项目所有的初始文件也可以看作源代码，里面有静态文件夹目录，模版文件夹目录，
项目配置文件和一个主要的入口文件index.rst。

* conf

conf文件里面的参数看起来很简单，下面是一些简单介绍::

    extensions = [
    ]

    #用于插入拓展模块的列表，比如我对于rst文件不是很熟手我想要使用markdown文件
    #执行
    #pip install --upgrade recommonmark
    #将recommonmark模块加入到列表中extensions = ['recommonmark']

    language = 'zh_CN'
    #用于设置文档界面语言，常用语言zh_CN – 简体中文、zh_TW – 繁体中文、en – 英语

    exclude_patterns = []
    #选择忽略文件的列表

    html_theme = 'classic'
    #主题选择

    templates_path = ['_templates',]
    #包含额外模版的列表文件夹路径

    html_static_path = ['_static']
    #存放静态文件的文件夹，将会复制到build之后的_static目录下


目前文档项目能使用的配置基本是这些，后面如果后续需求变化我会更新

* index.rst

这个文件是主要的文档首页入口
    主要就是这个toctree
    这个toctree主要的作用其实就是添加你当前source待编译目录下面的文档文件自动读取sphinx.rts和git.rst，自动将其标题生成目录添加到首页。
    如我目前的文档项目的index.rst
    ::

        .. toctree::
            :maxdepth: 2

            sphinx
            git

简单配置完成后执行
``sphinx-build -b html ./source ./build``
就会将source下的rst文档生成html项目此时就可以打开build下面的index.html进行预览

    参数介绍
        -b [参数]选择编译后的文件类型具体看 `-b参数介绍 <https://www.sphinx.org.cn/man/sphinx-build.html#cmdoption-sphinx-build-b>`_

        source 你要编译的文件路径

        build 编译后的文件夹路径

偶然收获
**********

在我整理文档的时候偶然发现sphinx的自动代码文档生成强大功能,比如python的代码注释文档生成功能
    假设在当前目录下有一个文件夹叫my_pro，下面有
    ::

            ├── build
            └── source
                ├── _static
                ├── _templates
                ├── conf.py
                ├── index.rst
            ├── my_pro
                ├── aa.py
                ├── bb.py

这是两个代码文件的内容

aa.py::

    """aa模块的文档注释"""


    class Aa(object):
        """
            Aa类的注释
        """

        @staticmethod
        def aa_api(x, y):
            """
            求商

            :param x: 整数
            :param y: 不能为零的整数
            :return: 两数之商
            """
            return x/y

bb.py::

    """Bb模块的文档注释"""


    class Bb(object):
    """
    Bb类的注释
    """

    @staticmethod
    def bb_api(x, y):
        """
        求商

        >>> Bb.bb_api(2, 4)
        0.5
        >>> Bb.bb_api(4, 2)
        2.0
        """
        return x/y


先配置一下conf.py,在两个地方需要配置::

    sys.path.insert(0, os.path.abspath('../my_pro'))
    extensions = [
        'sphinx.ext.autodoc',
    ]

之后再执行命令

``sphinx-apidoc -o ./source ./my_pro``

就会生成aa.rst，bb.rst，modules.rst三个文件

将modules添加到index的toctree::

    .. toctree::
        :maxdepth: 2

        sphinx
        git
        modules

执行build命令后将看到如下效果

.. image:: _static/images/index.png

* aa module

.. image:: _static/images/aa.png

* bb module

.. image:: _static/images/bb.png


看到这个自动生成代码文档功能的时候震惊了，写了那么久python居然不知道，以前的文档真是白写了，这功能真是太方便了。
最关键的是我发现代码文档不是简单的复制，而是以一种接口引用的方式，不用重复执行sphinx-apidoc去生成文档文件，
生成一次后，以后代码文档做了些许改动，执行build后就会自动同步，牛逼
