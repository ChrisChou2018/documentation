nodejs sdk AWSiot后台配置
=============================

入门
*********

node.js基础入门 `文档 <https://docs.aws.amazon.com/zh_cn/sdk-for-javascript/v2/developer-guide/creating-and-calling-service-objects.html>`_

`官方实例 <https://github.com/awslabs/aws-nodejs-sample.git>`_

首先要设置凭证用于登录aws iot，具体文档 `地址 <https://docs.aws.amazon.com/zh_cn/sdk-for-javascript/v2/developer-guide/getting-your-credentials.html>`_,
对于如何获取凭证 `在这 <https://docs.aws.amazon.com/zh_cn/sdk-for-javascript/v2/developer-guide/getting-your-credentials.html>`_
Linux、Unix 和 macOS 上的共享凭证文件：~/.aws/credentials，内容::

    [default]
    aws_access_key_id = <YOUR_ACCESS_KEY_ID>
    aws_secret_access_key = <YOUR_SECRET_ACCESS_KEY>


[default] 部分标题指定默认配置文件和凭证的相关值。您可以在同一共享配置文件中创建其他配置文件，每个配置文件都有自己的凭证信息。
以下示例显示了具有默认配置文件和两个其他配置文件的配置文件：
::

    [default] ; default profile
    aws_access_key_id = <DEFAULT_ACCESS_KEY_ID>
    aws_secret_access_key = <DEFAULT_SECRET_ACCESS_KEY>
        
    [personal-account] ; personal account profile
    aws_access_key_id = <PERSONAL_ACCESS_KEY_ID>
    aws_secret_access_key = <PERSONAL_SECRET_ACCESS_KEY>
        
    [work-account] ; work account profile
    aws_access_key_id = <WORK_ACCESS_KEY_ID>
    aws_secret_access_key = <WORK_SECRET_ACCESS_KEY>

可以明确选择开发工具包使用的配置文件，方法为在加载开发工具包之前设置 process.env.AWS_PROFILE，或者选择以下示例所示的凭证提供程序：
::

    var credentials = new AWS.SharedIniFileCredentials({profile: 'work-account'});
    AWS.config.credentials = credentials;

测试凭证是否正确：
::

    var AWS = require("aws-sdk");

    AWS.config.getCredentials(function(err) {
    if (err) console.log(err.stack);
    // credentials not loaded
    else {
        console.log("Access key:", AWS.config.credentials.accessKeyId);
        console.log("Secret access key:", AWS.config.credentials.secretAccessKey);
    }
    });

官方实例package.json
::

    {
        "dependencies": {},
        "name": "aws-nodejs-sample",
        "description": "A simple Node.js application illustrating usage of the AWS SDK for Node.js.",
        "version": "1.0.1",
        "main": "sample.js",
        "devDependencies": {},
        "scripts": {
            "test": "echo \"Error: no test specified\" && exit 1"
        },
        "author": "NAME",
        "license": "ISC"
    }

安装依赖包后会自动向改package.json中加入依赖文件名
::

    npm install aws-sdk
    npm install uuid # node8.0版本没有node-uuid模块所以需要自行安装



测试脚本
**********

创建名为 sample.js 的新文件以包含示例代码。首先，添加 require 函数调用以包括适用于 JavaScript 的开发工具包和 uuid 模块，使其可供您使用。

通过将唯一 ID 值附加到可识别的前缀（在本例中为 'node-sdk-sample-'），生成用于创建 Amazon S3 存储桶的唯一存储桶名称。
您可以通过调用 uuid 模块来生成唯一 ID。然后为用于将对象上传到存储桶的 Key 参数创建名称。

创建 promise 对象以调用 AWS.S3 服务对象的 createBucket 方法。收到成功响应时，创建将文本上传到新创建存储桶所需的参数。
使用另一个 promise，调用 putObject 方法来将文本对象上传到存储桶。

::

    // Load the SDK and UUID
    var AWS = require('aws-sdk');
    var uuid = require('uuid');

    // Create unique bucket name
    var bucketName = 'node-sdk-sample-' + uuid.v4();
    // Create name for uploaded object key
    var keyName = 'hello_world.txt';

    // Create a promise on S3 service object
    var bucketPromise = new AWS.S3({apiVersion: '2006-03-01'}).createBucket({Bucket: bucketName}).promise();

    // Handle promise fulfilled/rejected states
    bucketPromise.then(
    function(data) {
        // Create params for putObject call
        var objectParams = {Bucket: bucketName, Key: keyName, Body: 'Hello World!'};
        // Create object upload promise
        var uploadPromise = new AWS.S3({apiVersion: '2006-03-01'}).putObject(objectParams).promise();
        uploadPromise.then(
        function(data) {
            console.log("Successfully uploaded data to " + bucketName + "/" + keyName);
        });
    }).catch(
    function(err) {
        console.error(err, err.stack);
    });


执行脚本


关于配置
*********

配置 `详细说明 <https://docs.aws.amazon.com/zh_cn/sdk-for-javascript/v2/developer-guide/configuring-the-jssdk.html>`_


