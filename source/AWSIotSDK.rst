用JavaScript控制aws iot相关
==============================


配置登录信息
***************

首先配置登录认证和设置登录地区，我这里使用的是json配置信息::

    // 我的conf.json
    {
        "accessKeyId": "your accessKeyId", 
        "secretAccessKey": "your accessKey", 
        "region": "your login region"
    }

在导入aws-sdk的时候引入配置文件::

    var AWS = require('aws-sdk');
    AWS.config.loadFromPath('./conf.json');



策略相关
************

这里整理一些iot策略相关的接口代码

**展示当前iam 账户的所有策略**

::

    function listPolicy(){
        var params = {
        };
        iot.listPolicies(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }

**用策略名展示策略的详细**

::

    function getPolicyByName(){
        var params = {
            policyName: 'test_thing_chris-Policy' /* required */
        };
        iot.getPolicy(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else{
            console.log(data);
            }       // successful response
        });
    }


**通过策略版本ID获取某个版本的策略**

::

    function getPolicyByVersionID(versionid){
        var params = {
            policyName: 'test_thing_chris-Policy', /* required */
            policyVersionId: versionid /* required */
        };
        iot.getPolicyVersion(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }


**策略版本切换**

::

    // policy可以创建多个版本，在多个版本直接可以随时切换
    function switchPolicyVersion(policyName, versionID){
        var params = {
            policyName: policyName, /* required */
            policyVersionId: versionID /* required */
        };
        iot.setDefaultPolicyVersion(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }

**创建新的策略**

::

    // 创建一个新的策略
    function createPolicy(policyName, policyDoc){
        var params = {
            policyDocument: policyDoc, /* required */
            policyName: policyName /* required */
        };
        iot.createPolicy(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }

**创建一个策略新的策略版本分支**

::

    // 一个策略的版本上限是5个
    function createPolicyVersion(policyName, policyDoc){
        var params = {
            policyDocument: policyDoc, /* required */
            policyName: policyName, /* required */
            setAsDefault: true
        };
        iot.createPolicyVersion(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }

**列出所有的版本策略**

::

    // 列出策略所有的版本
    function listPoliciesVersion(policyName){
        var params = {
            policyName: policyName /* required */
        };
        iot.listPolicyVersions(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }

**删除策略**

::

    // 删除策略
    function deletePolicy(policyName){
        var params = {
            policyName: policyName /* required */
        };
        iot.deletePolicy(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }


**删除策略版本**
::

    // 删除指定策略的版本
    function deletePolicyVersion(policyName, versionID){
        var params = {
            policyName: policyName, /* required */
            policyVersionId: versionID /* required */
        };
        iot.deletePolicyVersion(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }



证书生成相关
***************

证书生成接口分创建证书和吊销证书，甚至是自定义CA证书和自定义csr设备证书签发，我这里首先记录
aws iot的标准证书的操作。



**创建证书**
::


    // 该方法请求证书创建并返回证书和密钥相关信息
    function createKeyAndCertificate(setAsActive){
        var params = {
            setAsActive: typeof setAsActive == "undefined" ? true : setAsActive
        };
        iot.createKeysAndCertificate(params, function(err, data) {
            // return: {certificateArn:'string', certificateId::'string', certificatePem:'string', keyPair:{PublicKey:'string', PrivateKey:'string'}} 
            if (err) console.log(err, err.stack); // an error occurred
            else{
                console.log(data);
                fs.writeFile('certificatePem.crt', data.certificatePem, function(err, data){
                    if(err){
                        console.log(err, err.stack);
                    }else{
                        console.log("crt file create suceess")
                    }
                });
                fs.writeFile('priviteKey.key', data.keyPair.PrivateKey, function(err, data){
                    if(err){
                        console.log(err, err.stack);
                    }else{
                        console.log("privitekey.key file create suceess")
                    }
                });
                fs.writeFile('puclicKey.key', data.keyPair.PublicKey, function(err, data){
                    if(err){
                        console.log(err, err.stack);
                    }else{
                        console.log("public key file create suceess")
                    }
                })
            }     // successful response
        });
    }


**将策略附加至证书**::

    function attachCertificateWithPolicy(policyName, ARN){
        var params = {
            policyName: policyName, /* required */
            principal: ARN /* required */
        };
        iot.attachPrincipalPolicy(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }


**将证书附加至Thing**::

    function attachThingWithCertificate(thingName, ARN){
        var params = {
            principal: ARN, /* required */
            thingName: thingName /* required */
        };
        iot.attachThingPrincipal(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }


**将证书解绑Thing**::

    function detachCertificateForThing(thingName, ARN){
        var params = {
            principal: ARN, /* required */
            thingName: thingName /* required */
        };
        iot.detachThingPrincipal(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }


**将证书解绑策略**::

    function detachPolicyForCertificate(policyName, ARN){
        var params = {
            policyName: policyName, /* required */
            principal: ARN /* required */
        };
        iot.detachPrincipalPolicy(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }


**删除证书,和转换证书状态**::

    // 证书不能直接删除，必须要经过转换状态为INACTIVE后

    function deletePolicy(certificateId){
        // delete policy befor Thing mast be inactive statu
        var params = {
            certificateId: certificateId, /* required */
            forceDelete: false // no matter is active or inactive or attach thing
        };
            iot.deleteCertificate(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    }

    function turnCertificateStatu(certificateId, statuCode){
        // param: statuCode ACTIVE | INACTIVE | REVOKED | PENDING_TRANSFER | REGISTER_INACTIVE | PENDING_ACTIVATION
        var params = {
            certificateId: certificateId, /* required */
            newStatus: statuCode /* required */
        };
        iot.updateCertificate(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else{
                console.log(data);
                deletePolicy(certificateId);
            }        // successful response
        });
    }



