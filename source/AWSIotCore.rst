关于AWSIotCore
====================


Thing（事务）
*************

连接到 AWS IoT 的设备在 AWS IoT 注册表中由事物 表示。事物 表示特定设备或逻辑实体。
它可以是物理设备或传感器（例如，灯泡或墙壁上的开关）。此外，它也可以是逻辑实体（如应用程序实例），
或没有连接到 AWS IoT 但与其他连接到 AWS IoT 的设备相关的物理实体。



策略
***********

策略就是设备权限许可，策略是用来附加到证书上的，用于授权设备，策略根据不同的消息传递方式附加
不同的身份 `认证类型 <https://docs.aws.amazon.com/zh_cn/iot/latest/developerguide/iot-authorization.html>`_

::

    # 这些是策略的类型
    iot:Connect     #表示连接到 AWS IoT Core 消息代理所需的权限
    iot:Publish     #代表向 MQTT 主题发布内容的权限
    iot:Receive     #表示从 AWS IoT Core 接收消息所需的权限
    iot:Subscribe   #代表订阅主题筛选条件的权限


**事物类型**

事务类型是用来存储与同一事务类型相关联的所有事务共有的描述和配置信息，这将简化 Registry 中的事物管理

1. 具有事物类型的事物最多可以具有 50 个属性。
2. 未设置事物类型的事物最多可以具有三个属性。
3. 一个事物只能与一种事物类型相关联。
4. 在账户中可以创建的事物类型数量不限。



**创建事务类型**

::

    aws iot create-thing-type \
    --thing-type-name "LightBulb" --thing-type-properties "thingTypeDescription=light bulb type, searchableAttributes=wattage,model"

    >>>>>
    {
        "thingTypeName": "LightBulb",
        "thingTypeId": "df9c2d8c-894d-46a9-8192-9068d01b2886",
        "thingTypeArn": "arn:aws:iot:us-west-2:123456789012:thingtype/LightBulb"
    }

**列出事务类型**

::

    aws iot list-thing-types

    >>>>>
    {
    "thingTypes": [
            {
                "thingTypeName": "LightBulb",
                "thingTypeProperties": {
                    "searchableAttributes": [
                        "wattage",
                        "model"
                    ],
                    "thingTypeDescription": "light bulb type"
                },
                "thingTypeMetadata": {
                    "deprecated": false,
                    "creationDate": 1468423800950
                }
            }
        ]
    }

**给事务类型添加描述**

::

    {
        "thingTypeProperties": {
            "searchableAttributes": [
                "model", 
                "wattage"
            ], 
            "thingTypeDescription": "light bulb type"
        }, 
        "thingTypeId": "df9c2d8c-894d-46a9-8192-9068d01b2886", 
        "thingTypeArn": "arn:aws:iot:us-west-2:123456789012:thingtype/LightBulb", 
        "thingTypeName": "LightBulb", 
        "thingTypeMetadata": {
            "deprecated": false, 
            "creationDate": 1544466338.399
        }
    }


**创建事务时时和事务类型创建关联**

*创建事物时，可以使用 CreateThing 命令指定事物类型*

::

    aws iot create-thing --thing-name "MyLightBulb" \
    --thing-type-name "LightBulb" --attribute-payload "{\"attributes\": {\"wattage\":\"75\", \"model\":\"123\"}}"

**更新事务类型**

::

    aws iot update-thing --thing-name "MyLightBulb" \
    --thing-type-name "LightBulb" --attribute-payload  "{\"attributes\": {\"wattage\":\"75\", \"model\":\"123\"}}"


**弃用事物类型**

*事物类型是不可变的。一旦定义了事务类型，便不可更改。然而，您可以通过弃用某种事物类型来防止用户将新事物与之关联。
所有与该事物类型相关联的现有事物将保持不变。*

::

    aws iot deprecate-thing-type --thing-type-name "myThingType"
    aws iot describe-thing-type --thing-type-name "StopLight" //查看结果
    
    >>>>>
    {
        "thingTypeName": "StopLight",
        "thingTypeProperties": {
            "searchableAttributes": [
                "wattage",
                "numOfLights",
                "model"
            ],
            "thingTypeDescription": "traffic light type",
        },
        "thingTypeMetadata": {
            "deprecated": true,
            "creationDate": 1468425854308,
            "deprecationDate": 1468446026349
        }
    }

    # 撤销弃用
    aws iot deprecate-thing-type --thing-type-name "myThingType" --undo-deprecate

    # 查看结果

    >>>>>
    {
        "thingTypeName": "StopLight",
        "thingTypeArn": "arn:aws:iot:us-east-1:123456789012:thingtype/StopLight",
        "thingTypeId": "12345678abcdefgh12345678ijklmnop12345678"
        "thingTypeProperties": {
            "searchableAttributes": [
                "wattage",
                "numOfLights",
                "model"
            ],
            "thingTypeDescription": "traffic light type"
        },
        "thingTypeMetadata": {
            "deprecated": false,
            "creationDate": 1468425854308,
        }
    }



通过组来管理事务
****************

通过将事物分类形成组，您能够使用事物组同时管理几个事物。组中还可以包含组 — 您可以构建组的层次结构。
您可以将策略附加到某个父组，该策略将由其所有子组和该组及其子组中的所有事物继承。这使得控制大量事物的权限非常简单。

个人理解：我能想到的应用场景就是每个普通用户的账号和事物组相关联，我通过他的事物组来找到相应的事务，来和事务取得关联。

::

    # 创建组命令
    aws iot create-thing-group --thing-group-name LightBulbs

    # 创建事物组时指定其父级：
    aws iot create-thing-group --thing-group-name RedLights --parent-group-name LightBulbs

    # 查看事务组的描述信息
    aws iot describe-thing-group --thing-group-name RedLights

    # 将事物添加到事物组
    aws iot add-thing-to-thing-group --thing-name MyLightBulb --thing-group-name RedLights

    # 从事物组中删除事物
    aws iot remove-thing-from-thing-group --thing-name MyLightBulb --thing-group-name RedLights

    # 列出事物组中的事物
    aws iot list-things-in-thing-group --thing-group-name LightBulbs

    # 列出事务组
    aws iot list-thing-groups

    # 使用可选的筛选条件列出具有指定组作为父级 (--parent-group) 的组
    aws iot list-thing-groups --parent-group LightBulbs

    # 将 --recursive 参数与 ListThingGroups 命令结合使用可以列出事物组的所有子组，而不仅仅是直接子组
    aws iot list-thing-groups --parent-group LightBulbs --recursive

    # 您可以使用 ListThingGroupsForThing 命令列出某个事物所属的组，包括任何父组
    aws iot list-thing-groups-for-thing --thing-name MyLightBulb

    # 更新事物组
    aws iot update-thing-group --thing-group-name "LightBulbs" --thing-group-properties \
    "thingGroupDescription=\"this is a test group\", attributePayload=\"{\"attributes\"={\"Owner\"=\"150\",\"modelNames\"=\"456\"}}"

    # 删除事物组
    aws iot delete-thing-group --thing-group-name "RedLights"


Topic(主题)
***************

主题决定了设备将要接收并执行的命令，这是一个双方共同约定的规则，也就是说这是用来标示命令类型的，
消息主题决定了设备如何去解读内容，而服务端也可以针对主题发送不同的命令消息，
主题是用来定义广播接收方式的，我们知道MQTT是发布订阅协议

`AWS iot 预留的主题 <https://docs.aws.amazon.com/zh_cn/iot/latest/developerguide/reserved-topics.html>`_


AWS Iot Core发送消息的方式
***************************

分为 `MQTT协议 <./MQTT.html>`_ 和 `MQTTwebSocket协议 <https://docs.aws.amazon.com/zh_cn/iot/latest/developerguide/mqtt-ws.html>`_

以及 `htmls协议 <https://docs.aws.amazon.com/zh_cn/iot/latest/developerguide/http.html>`_ 使用ssl协议


Rule(规则)
***************

定义规则决定了，服务端这边根据收到的信息执行什么样的动作，如：

1.补充或筛选从设备接收的数据。

2.将从设备接收的数据写入 Amazon DynamoDB 数据库。

3.将文件保存到 Amazon S3。

4.使用 Amazon SNS 向所有用户发送推送通知。

5.将数据发布到 Amazon SQS 队列。

6.调用 Lambda 函数来提取数据。

7.使用 Amazon Kinesis 处理来自大量设备的消息。

8.将数据发送到 Amazon Elasticsearch Service。

9.捕获 CloudWatch 指标。

10.更改 CloudWatch 警报。

11.将 MQTT 消息中的数据发送到 Amazon Machine Learning，以便根据 Amazon ML 模型进行预测。

12.向 Salesforce IoT 输入流发送消息。

13.将消息数据发送到 AWS IoT Analytics 通道。

14.开始执行 Step Functions 状态机。

15.将消息数据发送到 AWS IoT Events 输入。

16.将消息数据发送到 AWS IoT SiteWise 中的资产属性。

17.将消息数据发送到 Web 应用程序或服务。

AWS Lambda
***************

lambada是一种无服务的函数体，不需要服务器由一些特殊条件去触发执行，并按计算时长来计费

您可以使用 AWS Lambda 构建无服务器后端，以处理 Web、移动、物联网 (IoT) 和第 3 方 API 请求。
充分利用 Lambda 始终如一的高性能控制，如多内存配置和预配置并发，以便构建任何规模的延迟敏感应用程序。


Amazon API Gateway
**********************

Amazon API Gateway 是一项 AWS 服务，用于创建、发布、维护、监控和保护任意规模的 REST 和 WebSocket API。


