关于openSSL
====================


数字证书和签名
***************

**数字签名的作用在与用作验证数据是否被篡改**

对文件本身加密可能是个耗时过程，比如这封Email足够大，那么私钥加密整个文件以及拿到文件后的解密无疑是巨大的开销。
数字签名可以解决这个问题：
1.A先对这封Email执行哈希运算得到hash值简称“摘要”，取名h1
2.然后用自己私钥对摘要加密，生成的东西叫“数字签名”
3.把数字签名加在Email正文后面，一起发送给B
（当然，为了防止邮件被窃听你可以用继续公钥加密，这个不属于数字签名范畴）
4.B收到邮件后用A的公钥对数字签名解密，成功则代表Email确实来自A，失败说明有人冒充
5.B对邮件正文执行哈希运算得到hash值，取名h2
6.B 会对比第4步数字签名的hash值h1和自己运算得到的h2，一致则说明邮件未被篡改。

可以看出，对于一些不需要加密的明文数据但又想防止数据被篡改保证数据安全，就可以用数字签名


**数字证书的作用是进一步保障数字签名的安全与公钥安全**

假设如果有人用自己生成的公钥替换了你的公钥，在通过自己私钥去发送数据，这种情况下，原先的签名保障就失效了
而数字证书就是进一步保障签名的安全的。

那么数字证书是怎么生成的？以及如何配合数字签名工作？首先需要去找证书颁发机构certificate authority（CA）为公钥做认证。
证书中心用自己的私钥，对A的公钥和一些相关信息一起加密，生成"数字证书"（Digital Certificate）。
之后发送数据的时候就需要携带上正文，数字签名和数字证书。在取得内容的时候首先拿CA的公钥去解开数字证书拿到原本的公钥
再用公钥去对数字签名进行校验。（公钥一般在浏览器内的一些机构的根证书里面）



使用openssl生成公私钥
***************************************

**生成RSA密钥**

生成私钥::

    openssl genrsa -out rsa_private.key 2048

生成公钥::

    openssl rsa -in rsa_private.key -pubout -out rsa_public.key

生成RSA私钥(使用aes256加密)::

    openssl genrsa -aes256 -passout pass:111111 -out rsa_aes_private.key 2048
    # 其中 passout 代替shell 进行密码输入，否则会提示输入密码

生成加密后的内容如::

    -----BEGIN RSA PRIVATE KEY-----
    Proc-Type: 4,ENCRYPTED
    DEK-Info: AES-256-CBC,5584D000DDDD53DD5B12AE935F05A007
    Base64 Encoded Data
    -----END RSA PRIVATE KEY-----

此时若生成公钥，需要提供密码::

    openssl rsa -in rsa_aes_private.key -passin pass:111111 -pubout -out rsa_public.key
    # passin 代替shell 进行密码输入，否则会提示输入密码


转换命令
**********

私钥转非加密::

    openssl rsa -in rsa_aes_private.key -passin pass:111111 -out rsa_private.key

私钥转加密::

    openssl rsa -in rsa_private.key -aes256 -passout pass:111111 -out rsa_aes_private.key

私钥PEM转DER::

    openssl rsa -in rsa_private.key -outform der-out rsa_aes_private.der
    # -inform和-outform 参数制定输入输出格式，由der转pem格式同理

查看私钥明细::

    openssl rsa -in rsa_private.key -noout -text
    # 再加上-pubin参数可查看公钥明细



自签名证书和CA签名证书区别
****************************


自签名的证书无法被吊销，CA签名的证书可以被吊销 能不能吊销证书的区别在于，如果你的私钥被黑客获取，
如果证书不能被吊销，则黑客可以伪装成你与用户进行通信

如果你的规划需要创建多个证书，那么使用私有CA的方法比较合适，因为只要给所有的客户端都安装了CA的证书，
那么以该证书签名过的证书，客户端都是信任的，也就是安装一次就够了。
如果你直接用自签名证书，你需要给所有的客户端安装该证书才会被信任，如果你需要第二个证书，则还的挨个给所有的客户端安装证书2才会被信任。


证书类型：
***********

x509的证书编码格式有两种

**1.**
PEM(Privacy-enhanced Electronic Mail) 是明文格式的  以 -----BEGIN CERTIFICATE-----开头，
已-----END CERTIFICATE-----结尾，中间是经过base64编码的内容,
apache需要的证书就是这类编码的证书 查看这类证书的信息的命令为 ：openssl x509 -noout -text -in server.pem
其实PEM就是把DER的内容进行了一次base64编码

**2.**
DER 是二进制格式的证书，
查看这类证书的信息的命令为 ：openssl x509 -noout -text -inform der -in server.der

拓展名：
*********

1. .crt 证书文件 ，可以是DER（二进制）编码的，也可以是PEM（ ASCII (Base64) ）编码的 ，在类unix系统中比较常见
2. .cer 也是证书  常见于Windows系统  编码类型同样可以是DER或者PEM的，windows 下有工具可以转换crt到cer
3. .csr 证书签名请求   一般是生成请求以后发送给CA，然后CA会给你签名并发回证书
4. .key  一般公钥或者密钥都会用这种扩展名，可以是DER编码的或者是PEM编码的
5. .p12 证书  包含一个X509证书和一个被密码保护的私钥

::

    查看DER编码的（公钥或者密钥）的文件的命令为 openssl rsa -inform DER  -noout -text -in  xxx.key
    查看PEM编码的（公钥或者密钥）的文件的命令为 openssl rsa -inform PEM   -noout -text -in  xxx.key


证书格式转换：
***************

**查看证书细节**::

    openssl x509 -in cert.crt -noout -text

**转换证书编码格式**::

    openssl x509 -in cert.cer -inform DER -outform PEM -out cert.pem

**合成 pkcs#12 证书(含私钥)**

1. 将 pem 证书和私钥转 pkcs#12 证书::

    openssl pkcs12 -export -in server.crt -inkey server.key
    -passin pass:111111 -password pass:111111 -out server.p12

2. 将 pem 证书和私钥/CA 证书 合成pkcs#12 证书::

    openssl pkcs12 -export -in server.crt -inkey server.key -passin pass:111111 \
    -chain -CAfile ca.crt -password pass:111111 -out server-all.p12

**从PKCS12证书文件中解压证书**

::

    penssl pkcs12 -in server.p12 -password pass:111111 -passout pass:111111 -out out/server.pem



直接生成私钥再生成自签名证书
*****************************
::

    openssl req -newkey rsa:2048 -nodes -keyout rsa_private.key -x509 -days 365 -out cert.crt

req是证书请求的子命令，-newkey rsa:2048 -keyout private_key.pem 表示生成私钥(PKCS8格式)
-nodes 表示私钥不加密，若不带参数将提示输入密码；
-x509表示输出证书;
-days365 为有效期，此后根据提示输入证书拥有者信息；


若执行自动输入证书拥有者信息，可使用-subj选项::

    openssl req -newkey rsa:2048 -nodes -keyout rsa_private.key -x509 -days 365 -out cert.crt
    -subj "/C=CN/ST=GD/L=SZ/O=vihoo/OU=dev/CN=vivo.com/emailAddress=yy@vivo.com"


使用 已有RSA 私钥生成自签名证书
********************************
::

    openssl req -new -x509 -days 365 -key rsa_private.key -out cert.crt
    # -new 指生成证书请求，加上-x509 表示直接输出证书，
    # -key 指定私钥文件，其余选项与上述命令相同


生成签名请求及CA签名
*********************

使用 RSA私钥生成 CSR 证书请求文件（题外话：可以用来和AWS iot的注册CA证书进行签名加密）::

    openssl genrsa -aes256 -passout pass:111111 -out server.key 2048
    openssl req -new -key server.key -out server.csr

    openssl req -new -key server.key -passin pass:111111 -out server.csr
    -subj "/C=CN/ST=GD/L=SZ/O=vihoo/OU=dev/CN=vivo.com/emailAddress=yy@vivo.com"
    # 带上密码和证书拥有者信息

** 此时生成的 csr签名请求文件可提交至CA进行签发 **

查看CSR 的细节::

    cat server.csr
    -----BEGIN CERTIFICATE REQUEST-----
    Base64EncodedData
    -----END CERTIFICATE REQUEST-----

    openssl req -noout -text -in server.csr

CSR里面包含有请求发起者的身份信息、用来对此请求进行验真的的公钥以及所请求证书专有名称，
CSR里还可能带有CA要求的其它有关身份证明的信息。


CA机构申请证书流程
*******************

a. 服务方 S 向第三方机构CA提交公钥、组织信息、个人信息(域名)等信息并申请认证；

b. CA 通过线上、线下等多种手段验证申请者提供信息的真实性，如组织是否存在、企业是否合法，是否拥有域名的所有权等；

c. 如信息审核通过，CA 会向申请者签发认证文件-证书。

证书包含以下信息：申请者公钥、申请者的组织信息和个人信息、签发机构 CA 的信息、有效时间、证书序列号等信息的明文，同时包含一个签名；

签名的产生算法：首先，使用散列函数计算公开的明文信息的信息摘要，然后，采用 CA 的私钥对信息摘要进行加密，密文即签名；

d. 客户端 C 向服务器 S 发出请求时，S 返回证书文件；

e. 客户端 C 读取证书中的相关的明文信息，采用相同的散列函数计算得到信息摘要，然后，利用对应 CA 的公钥解密签名数据，对比证书的信息摘要，如果一致，则可以确认证书的合法性，即公钥合法；

f. 客户端然后验证证书相关的域名信息、有效时间等信息；

g. 客户端会内置信任 CA 的证书信息(包含公钥)，如果CA不被信任，则找不到对应 CA 的证书，证书也会被判定非法。

在这个过程注意几点：

1. 申请证书不需要提供私钥，确保私钥永远只能服务器掌握；

2. 证书的合法性仍然依赖于非对称加密算法，证书主要是增加了服务器信息以及签名；

3. 内置 CA 对应的证书称为根证书，颁发者和使用者相同，自己为自己签名，即自签名证书；

4. 证书=公钥+申请者与颁发者信息+签名；


**使用 CA 证书及CA密钥 对请求签发证书进行签发，生成 x509证书**

在X.509里，组织机构通过发起证书签名请求(CSR)来得到一份签名的证书。
首先需要生成一对钥匙对，然后用其中的私钥对CSR进行签名，并安全地保存私钥。
CSR进而包含有请求发起者的身份信息、用来对此请求进行验真的的公钥以及所请求证书专有名称。
CSR里还可能带有CA要求的其它有关身份证明的信息。
然后CA对这个专有名称发布一份证书，并绑定一个公钥. 
组织机构可以把受信的根证书分发给所有的成员，这样就可以使用公司的PKI系统了。
像Firefox, IE, Opera, Safari 以及Google Chrome都预装有早就确定的根证书列表，
所以使用主流CA发布的证书SSL都直接可以正常使用。浏览器的开发者直接影响着它的用户对第三方的信任。
FireFox就提供了一份csv/html格式的列表X.509也定义了CRL实现标准。另一种检查合法性的方式是OCSP。
FireFox 3开始就默认打开了这项检查功能，Windows从Vista版本以后也一样。

::

    openssl x509 -req -days 3650 -in server.csr -CA ca.crt -CAkey ca.key
    -passin pass:111111 -CAcreateserial -out server.crt


引用文章 `链接 <https://www.cnblogs.com/littleatp/p/5878763.html>`_

