SSH相关
===============

*用于远程登录Linux最常用的命令，不过最近使用的客户端比较多，渐渐的忘掉一些命令了，现在这里整理*

基础登录命令

`ssh [-p port] user@remote`

需要确保服务端的ssh服务时开启的并配置可登录

`sudo service ssh start`


SSH免密登录设置
*************************

第一步，根据DSA算法配置公钥::

    [root@localhost ~]# cd /root/.ssh/             【root用户就在root目录下的.ssh目录】
    [root@localhost ~]# cd /home/omd/.ssh/   【普通用户就是在家目录下的.ssh目录】

    [root@localhost .ssh]# ssh-keygen -t dsa     # 一路回车即可
    id_dsa         -->私钥(钥匙)
    id_dsa.pub     -->公钥(锁)

.. image:: _static/images/ssh1.png

第二部，将公钥上传至服务器::

    [root@localhost .ssh]# ssh-copy-id -i id_dsa.pub –p 22 omd@192.168.25.110          【 使用ssh登录的默认端口22】

尝试登录服务器是否免密
`ssh -p 22 username@remote ip`

