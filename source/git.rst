git学习笔记整理
===============

*git笔记整理*

我与git的日子：
***************

git是我第一个学习并使用的分布式版本管理系统，虽然后来工作当中用svn的比较多，因为windows上的TortoiseSvn的系统集成度更好，
而且工具免费，使用也比较简单。这两个我也比较过，我不能说谁更好吧，git虽然速度快，优雅，但是svn比较简单便捷，在不同的场景
各有所长我觉得。

对了git有一点我非常喜欢，它对于VsCode编辑器的集成非常棒，即时的代码状态反馈，以及冲突编辑，代码提交我都非常喜欢

git日常使用：
***************

::

  git init
  # 在一个项目目录中初始化一个git仓库执行命令
  # 之后会在当前仓库中生成.git文件夹，默认是不可见的，这个文件夹会存储所有的git仓库状态文件

  git add *
  # 添加所有文件到git仓库

  git add *.py
  # 添加所有以.py为结尾的文件到git仓库
  # add 后面可以是某个文件名，也可以是通配符

  git commit -m '代码提交记录'
  # 在执行完commit提交提交命令后才是真的将文件推送到版本库，add只是将文件推送到了暂存区

  git commit --amend -m '新信息'
  # 修改最新提交的信息

  git rebase -i HEAD~3
  # 修改历史的三条提交信息

  git rebase -i 提交的key值前7位
  # 可以使用git log --oneline 查看提交的对应key值，
  # 这个命令将用来确定commit范围，表示从此提交开始到当前的提交的commit日志修改。

  git clone git://git仓库地址
  # 将拉取远程代码库的代码到当前所在的文件夹

  git status
  # 查看当前项目状态，比如以这个项目为准，在刚执行完git init后，再执行当前命令就会看到如下

.. image:: _static/images/git_status1.png

此时再执行git add * 将所有的文件添加到暂存区后执行git status

.. image:: _static/images/git_status2.png

最后执行git commit -m '初始化仓库'提交所有更改到主分支，再执行git status你会发现为空，当你修改文件或者添加了新的文件
并将其add添加到暂存区的时候再执行git status的时候

.. image:: _static/images/git_status3.png

你会看到新添加的文件或者修改的文件显示在状态列表中，非常方便nice

**git status补充**

git status -s 简约的图标标记仓库状态列表

如未添加到暂存区文件和新增文件1.py

.. image:: _static/images/git_status4.png

添加到暂存区的修改文件和新增的文件

.. image:: _static/images/git_status5.png

A开头的是添加到暂存区新增的文件，绿色的M是已添加暂存区的修改文件

git status -b -s 可以已简约模式显示当前所在的分支和所有状态信息

**查看修改**

1. git diff 查看所有未添加到暂存区的修改
2. git diff <文件路径> 查看某个文件暂未提交到暂存区的修改
3. git diff --cached <文件路径> 查看当前文件提交到暂存区的修改
4. git diff --chched 查看所有提交到暂存区的修改
5. git diff 版本号码1 版本号码2 查看两个提交之间的差异，注意先后顺序，第一个为老的版本，第二个为新的改动

**撤销暂存区修改**

1. git reset HEAD <文件路径> 撤销当前文件提交的暂存区修改
2. git reset HEAD 撤销所有暂存区的修改

**删除文件**

1. git rm <文件路径> 将文件从暂存区和磁盘上都删除
2. git rm --cached 将文件从暂存区删除，但是保留磁盘


**查看历史提交**

1. git log 查看所有的提交详细记录
2. git log --oneline 查看简要的提交记录
3. git log --oneline --graph 开启拓展图的模式，以更好的显示合并记录
4. git log --reverse --oneline 以倒叙的方式查看记录
5. git log --author=Linus --oneline -5 查看用户为Linus的提交的5条记录
6. git show <commit log key> 查看某次提交的更改

*特殊过滤*::

  git log --oneline --before={3.weeks.ago} --after={2010-04-18} --no-merges
  # 查看三周前且2010/4/18日后的提交记录，且不显示合并记录


git分支管理：
***************

**创建分支**

1. git branch <分支名称> 创建一个新的分支。
2. git branch 列出所有的分支。

**切换分支**

1. git checkout <分支名称> 切换分支,Git 会用该分支的最后提交的快照替换你的工作目录的内容， 所以多个分支不需要多个目录。
2. git checkout -b <分支名称> 创建分支并切换到这个分支。
3. git checkout -b mywork origin 基于远程origin库创建一个mywork分支

**合并分支**

git merge <分支名称> 合并分支到主要分支

**删除分支**

git branch -d <分支名称> 删除分支

**生产环境分支管理的实例**

`引文 <https://git-scm.com/book/zh/v2/Git-分支-分支的新建与合并>`_


git标签：
***************

定义:
当一个提交达到了一个比较重要的阶段的时候，比如版本号达到了release阶段就可以给一个提交打上标签
::

  git tag 'v1.0' # 当前最新提交添加v1.0的tag
  git tag -a 'v1.0' # 当前最新提交添加一个代注释的标签，会进入vi编辑模式要求注释
  git tag -a 'v0.01' -m 'tag注释'# 打上标签同时顺便带上注释
  git tag -a v0.9 # <提交记录key> 给特定的提交记录打上标签
  git tag -l # 查看所有的标签
  git tag -l 'v*' # 所有以v开头的标签
  git tag -d 'tag名称' # 删除对应标签
  git show <tag名称> # 显示当前tag的详细相关
  git push origin <tagName> # 将单个tag同步到远程服务器
  git push origin --tags #推送所有的tag到远程服务器
  git checkout <tag名称> # 以这个tag为准检出一个分支


git撤销修改:
*************
::

  git reset --hard HEAD 将当前版本重置为 HEAD（用于 merge 失败）
  git reset --hard <commit> 将当前版本重置至某一个提交状态(慎用!)
  git reset <commit> 将当前版本重置至某一个提交状态，代码不变
  git reset --merge <commit> 重置至某一状态,保留版本库中不同的文件
  git reset --keep <commit> 重置至某一状态,重置变化的文件,代码改变
  git checkout HEAD <file> 丢弃本地更改信息并将其存入特定文件
  git revert <commit> 撤消提交


git rebase:
*************

看到这个命令的说明我还是很迷的，因为我在生产环境中大多数都是用svn，从来也没有遇上过这种使用场景，
它可以使多个commit合并为一个commit,我唯一可以想到的作用或许就是保持commit库整洁吧

*以下是我引用他人的例子，我本来打算自己执行命令然后再记录，
但是我在创建分支后尝试合并commit再merge到master分支，经过一些误操作导致库被破坏，
经过几次尝试我发现这个命令还是很危险的命令，使用不好的话会污染commit库，感觉非常危险，
在我找到诀窍和使用场景前我还是要慎用*


rebase合并commit命令：
************************
::

  git rebase -i  [startpoint]  [endpoint]
  # 其中-i的意思是--interactive，即弹出交互式的界面让用户编辑完成合并操作，
  # [startpoint] [endpoint]则指定了一个编辑区间，如果不指定[endpoint]，
  # 则该区间的终点默认是当前分支HEAD所指向的commit(注：该区间指定的是一个前开后闭的区间)。

先执行git log结果如下

.. image:: _static/images/rebase1.png

我们的目标是把最后三个提交合并为一个提交：

执行

`git rebase -i 36224db`

.. image:: _static/images/rebase2.png

上面未被注释的部分列出的是我们本次rebase操作包含的所有提交，下面注释部分是git为我们提供的命令说明。
每一个commit id 前面的pick表示指令类型，git 为我们提供了以下几个命令
::

  # pick：保留该commit（缩写:p）
  # reword：保留该commit，但我需要修改该commit的注释（缩写:r）
  # edit：保留该commit, 但我要停下来修改该提交(不仅仅修改注释)（缩写:e）
  # squash：将该commit和前一个commit合并（缩写:s）
  # fixup：将该commit和前一个commit合并，但我不要保留该提交的注释信息（缩写:f）
  # exec：执行shell命令（缩写:x）
  # drop：我要丢弃该commit（缩写:d）

将commit内容编辑如下:

.. image:: _static/images/rebase3.png

然后是注释修改界面:

.. image:: _static/images/rebase4.png

编辑完保存即可完成commit的合并了：

.. image:: _static/images/rebase5.png


rebase commit粘贴命令：
*************************

当项目中存在多个分支，有时候需要将某一个分支中的一段提交同时应用到其他分支中，就像下图：

.. image:: _static/images/rebasec1.png

我希望将develop分支中的C~E部分复制到master分支中，这时我们就可以通过rebase命令来实现
（如果只是复制某一两个提交到其他分支，建议使用更简单的命令:git cherry-pick）。
在实际模拟中，我创建了master和develop两个分支:

**master分支**

.. image:: _static/images/rebasec2.png

**develop分支**

.. image:: _static/images/rebasec3.png

我们使用命令的形式为::

  git rebase   [startpoint]   [endpoint]  --onto  [branchName]

其中，[startpoint]  [endpoint]仍然和上一个命令一样指定了一个编辑区间(前开后闭)，
--onto的意思是要将该指定的提交复制到哪个分支上。
所以，在找到C(90bc0045b)和E(5de0da9f2)的提交id后，我们运行以下命令
::

  git  rebase   90bc0045b^   5de0da9f2   --onto master

注:因为[startpoint]  [endpoint]指定的是一个前开后闭的区间，为了让这个区间包含C提交，我们将区间起始点向后退了一步。
运行完成后查看当前分支的日志:

.. image:: _static/images/rebasec4.png

可以看到，C~E部分的提交内容已经复制到了G的后面了，大功告成？NO！我们看一下当前分支的状态:

.. image:: _static/images/rebasec5.png

当前HEAD处于游离状态，实际上，此时所有分支的状态应该是这样:

.. image:: _static/images/rebasec6.png

所以，虽然此时HEAD所指向的内容正是我们所需要的，但是master分支是没有任何变化的，
git只是将C~E部分的提交内容复制一份粘贴到了master所指向的提交后面，
我们需要做的就是将master所指向的提交id设置为当前HEAD所指向的提交id就可以了，即:
::

  git checkout master
  git reset --hard  0c72e64

.. image:: _static/images/rebasec7.png


merge和rebase的优缺点分析:
***************************

我觉得这两个命令虽然都有合并的作用，但是又完全不向干涉，一个是用来合并独立出来的分支，
一个是合并当前分支的多个commit，运用场景我觉得是不同的，类似与rebase是将树的多个枝干进行合并或嫁接到另一棵树
而merge将两棵树合并为一棵树。rebase更像是merge的一个微操作。其优缺点比较明显，merge植树，rebase修理枝梢，
其更深层次的理念我还需要在实际工作应用中进行挖掘

git bundle(补丁):
***********************

patch是布丁的意思，git原来功能这么多样化，简直瑞士军刀一般，比起简单粗暴的svn感觉有些小复杂。
git format-patch命令可以将commit和其信息生成为一个补丁文件.patch，这个.patch是Git专用的补丁文件

**git patch基本命令**::

  git format-patch 【commit key】-n
  # -n表示包含这个key之前的几条记录打包为补丁，如果只是当前commit key记录为1就好了

生成了包含当前key的patch，一共1个，命名也有规律000X+commit的内容.patch
我们把这个文件移到一个新的文件夹比如~/patch/patch/中。

此时切换到其他需要打补丁的分支，假设为master


**不需要补丁commit详细信息只需要更改**::

  git apply --check ~/patch/patch/*.patch #检查补丁是否可用
  git apply ~/patch/patch/*.patch

假设没有冲突成功了，这个时候改动就保存在了工作区，需要手动再commit，并填上注释

**原封不动的完整commit包含注释和更改信息**::

  git apply --check ~/patch/patch/*.patch #检查补丁是否可用
  git am ~/patch/patch/*.patch

此时连commit都不需要直接将完整的commit打入分支（假设没有冲突）


**patch补丁有冲突**

当你两个分支同时更改一个文件的时候，文件不一致时候，并都commit了，此时将其中一个分支的更改次文件的补丁，运用
于另一个分支，如果文件中的内容有错行，和行内更改极有可能发生冲突

首先校验补丁文件的时候就会提示你次补丁有问题

执行git am 某个补丁.patch,就会看到以下信息，就是表示可能有冲突::

  error: 打补丁失败：source/git.rst:304
  error: source/git.rst：补丁未应用

如果继续执行大补丁操作::

  应用：apply patch
  error: 打补丁失败：source/git.rst:304
  error: source/git.rst：补丁未应用
  打补丁失败于 0001 apply patch
  提示：用 'git am --show-current-patch' 命令查看失败的补丁
  当您解决这一问题，执行 "git am --continue"。
  如果您想要跳过这一补丁，则执行 "git am --skip"。
  若要复原至原始分支并停止补丁操作，执行 "git am --abort"。

手动解决冲突的地方，填补错行的地方，修改行内冲突后执行,参照补丁文件修改后::

  git am --continue

后解决并保留完整的commit


**git bundle**

将git打包，并可以将其分享给其他人，网上似乎很少有关于bundle的描述，我在国内社区化化组汉化的官方定义上是这样描述的，
如果你的另一台电脑无法通讯，或者网络中断没有共享服务器的权限，你又希望通过邮件将更新发送给别人，
却不希望通过 format-patch 的方式传输 40 个提交。这些情况下 git bundle 就会很有用。
bundle 命令会将 git push 命令所传输的所有内容打包成一个二进制文件， 你可以将这个文件通过邮件或者闪存传给其他人，然后解包到其他的仓库中。

::

  git bundle create <file> <git-rev-list-args>
  # 语法

  git bundle create repo.bundle HEAD master
  # 然后你就会有一个名为 repo.bundle 的文件，该文件包含了所有重建该仓库 master 分支所需的数据。
  # 在使用 bundle 命令时，你需要列出所有你希望打包的引用或者提交的区间。
  # 如果你希望这个仓库可以在别处被克隆，你应该像例子中那样增加一个 HEAD 引用

  git clone repo.bundle repo
  # 假设别人传给你一个 repo.bundle 文件并希望你在这个项目上工作。
  # 你可以从这个二进制文件中克隆出一个目录，就像从一个 URL 克隆一样
  # 如果你在打包时没有包含 HEAD 引用，你还需要在命令后指定一个 -b master
  # 或者其他被引入的分支， 否则 Git 不知道应该检出哪一个分支。

现在假设你提交了 3 个修订，并且要用邮件或者U盘将新的提交放在一个包里传回去。::

  $ git log --oneline
  71b84da last commit - second repo
  c99cf5b fourth commit - second repo
  7011d3d third commit - second repo
  9a466c5 second commit
  b1ec324 first commit

为了实现这个目标，你需要计算出差别。 就像我们在 提交区间 介绍的，你有很多种方式去指明一个提交区间。
我们可以使用 origin/master..master 或者 master ^origin/master 之类的方法 来获取那
3 个在我们的 master 分支而不在原始仓库中的提交。 你可以用 log 命令来测试。
::

  $ git log --oneline master ^9a466c5
  71b84da last commit - second repo
  c99cf5b fourth commit - second repo
  7011d3d third commit - second repo

这样就获取到希望被打包的提交列表，将这些提交打包。
我们可以用 git bundle create 命令，加上我们想用的文件名，以及要打包的提交区间。
::

  $ git bundle create commits.bundle master ^9a466c5
  Counting objects: 11, done.
  Delta compression using up to 2 threads.
  Compressing objects: 100% (3/3), done.
  Writing objects: 100% (9/9), 775 bytes, done.
  Total 9 (delta 0), reused 0 (delta 0)

**校验包文件**

`git bundle verify *.bundle`

假设包是合法的，所以可以从这个包里提取出提交。如果想查看这边包里可以导入哪些分支，同样有一个命令可以列出这些顶端：

`git bundle list-heads *.bundle`

可以使用 fetch 或者 pull 命令从包中导入提交。 这里要从包中取出 master 分支到仓库中的 other-master 分支：

::

  $ git fetch ../commits.bundle master:other-master
  From ../commits.bundle
  * [new branch]      master     -> other-master

完，这个bundle其实就是在网络不通的情况下，将原本可以git clone origin 或者git pull origin，的操作，本地化，先
将需要拉取或者克隆的库打包，再本地克隆或者提取，感觉有点方便。。

