npm包管理笔记
==============

*单独整理npm的一些技巧*


初步使用
********

国内更换淘宝镜像源


**方法一**

在系统的HOME目录新建.npmrc文件并添加 registry = https://registry.npm.taobao.org


**方法二**

你可以使用淘宝定制的 cnpm 命令行工具代替默认的 npm:

::

    npm install -g cnpm --registry=https://registry.npm.taobao.org
    //之后即可使用cnpm来安装包
    cnpm install <包>

**本地安装包**::

    npm install <包>      # 本地安装
    # 或者
    npm i <包>

**全局安装**::

    npm install <包> -g   # 全局安装


**卸载安装**::

    npm uninstall <包名>

**更新包**::

    //更新当前项目中安装的某个包
    npm update <包名>

    //更新当前项目中安装的所有包
    npm update

    //更新全局安装的包
    npm update <包名> -g


**查看包安装的位置**

`npm prefix -g`

**搜索包**

`npm search <关键字>`


使用 package.json
##################

当你的项目需要依赖多个包时，推荐使用 package.json。其优点为：

它以文档的形式规定了项目所依赖的包
可以确定每个包所使用的版本
项目的构建可以重复，在多人协作时更加方便
创建package.json文件

**手动创建**
或者 通过 npm init 命令生成遵守规范的 package.json文件
文件中必须包含： name 和 version

**指定依赖包**

两种依赖包：

dependencies: 在生产环境中需要依赖的包。通过npm install <packge>
--save命令自动添加依赖到文件（或者使用简写的参数 -S）。

devDependencies：仅在开发和测试环节中需要依赖的包。
通过npm install <packge> --save-dev命令自动添加依赖到文件（或者使用简写的参数 -D）。

**设置默认配置**

使用 npm set 命令用来设置环境变量。

也可以用它来为 npm init设置默认值，这些值会保存在 ~/.npmrc文件中。::

    $ npm set init-author-name 'Your name'
    $ npm set init-author-email 'Your email'
    $ npm set init-author-url 'http://yourdomain.com'
    $ npm set init-license 'MIT'

**更改全局安装目录**

使用npm config命令可以达到此目的。::

    npm config set prefix <目录>

或者手动在 ~/.npmrc文件中进行配置：

::

    prefix = /home/yourUsername/npm

更改目录后记得在系统环境变量 PATH中添加该路径：

::

    # .bashrc 文件
    export PATH=~/npm/bin:$PATH

