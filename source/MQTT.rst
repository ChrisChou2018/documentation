MQTT发布订阅式消息传输协议
===========================

MQTT是基于二进制消息的发布/订阅编程模式的消息协议，最早由IBM提出的，如今已经成为OASIS规范。由于规范很简单，非常适合需要低功耗和网络带宽有限的IoT场景，比如：

1. 遥感数据
2. 汽车
3. 智能家居
4. 智慧城市
5. 医疗医护

由于物联网的环境是非常特别的，所以MQTT遵循以下设计原则：

1. 精简，不添加可有可无的功能。
2. 发布/订阅（Pub/Sub）模式，方便消息在传感器之间传递。
3. 允许用户动态创建主题，零运维成本。
4. 把传输量降到最低以提高传输效率。
5. 把低带宽、高延迟、不稳定的网络等因素考虑在内。
6. 支持连续的会话控制。
7. 理解客户端计算能力可能很低。
8. 提供服务质量管理。
9. 假设数据不可知，不强求传输数据的类型与格式，保持灵活性。


**主题**

MQTT是通过主题对消息进行分类的，本质上就是一个UTF-8的字符串，不过可以通过反斜杠表示多个层级关系。主题并不需要创建，直接使用就是了。

主题还可以通过通配符进行过滤。其中，+可以过滤一个层级，而*只能出现在主题最后表示过滤任意级别的层级。举个例子：

::

    building-b/floor-5：代表B楼5层的设备。

    +/floor-5：代表任何一个楼的5层的设备。

    building-b/*：代表B楼所有的设备。

注意，MQTT允许使用通配符订阅主题，但是并不允许使用通配符广播。


**服务质量**

为了满足不同的场景，MQTT支持三种不同级别的服务质量（Quality of Service，QoS）为不同场景提供消息可靠性：

1. 级别0：尽力而为。消息发送者会想尽办法发送消息，但是遇到意外并不会重试。

2. 级别1：至少一次。消息接收者如果没有知会或者知会本身丢失，消息发送者会再次发送以保证消息接收者至少会收到一次，当然可能造成重复消息。

3. 级别2：恰好一次。保证这种语义肯待会减少并发或者增加延时，不过丢失或者重复消息是不可接受的时候，级别2是最合适的。
服务质量是个老话题了。级别2所提供的不重不丢很多情况下是最理想的，不过往返多次的确认一定对并发和延迟带来影响。
级别1提供的至少一次语义在日志处理这种场景下是完全OK的，所以像Kafka这类的系统利用这一特点减少确认从而大大提高了并发。
级别0适合鸡肋数据场景，食之无味弃之可惜，就这么着吧。


**消息类型**

MQTT拥有14种不同的消息类型：

CONNECT：客户端连接到MQTT代理

CONNACK：连接确认

PUBLISH：新发布消息

PUBACK：新发布消息确认，是QoS 1给PUBLISH消息的回复

PUBREC：QoS 2消息流的第一部分，表示消息发布已记录

PUBREL：QoS 2消息流的第二部分，表示消息发布已释放

PUBCOMP：QoS 2消息流的第三部分，表示消息发布完成

SUBSCRIBE：客户端订阅某个主题

SUBACK：对于SUBSCRIBE消息的确认

UNSUBSCRIBE：客户端终止订阅的消息

UNSUBACK：对于UNSUBSCRIBE消息的确认

PINGREQ：心跳

PINGRESP：确认心跳

DISCONNECT：客户端终止连接前优雅地通知MQTT代理



**MQTT代理**

市面上有相当多的高质量MQTT代理，其中mosquitto是一个开源的轻量级的C实现，完全兼容了MQTT 3.1和MQTT 3.1.1。
下面就以mosquitto为例演示一下MQTT的使用。环境是百度开放云的云服务器以及Ubuntu 14.04.1 LTS，
简单起见MQTT代理和客户端都安装在同一台云服务器上了。

首先SSH到云服务器，安装mosquitto以及搭配的客户端：
::

    apt-get install mosquitto
    apt-get install mosquitto-clients


现在在云端模拟云服务，订阅某办公楼5层的温度作为主题：
::

    mosquitto_sub -d -t 'floor-5/temperature'
    Received CONNACK
    Received SUBACK
    Subscribed (mid: 1): 0

然后另外打开一个SSH连接，模拟温度计发送温度消息：
::

    mosquitto_pub -d -t 'floor-5/temperature' -m '15'
    Received CONNACK
    Sending PUBLISH (d0, q0, r0, m1, 'floor-5/temperature', ... (2 bytes))

此时回到第一个SSH客户端可以看到信息已经接收到了，之后便是心跳消息：
::

    Received PUBLISH (d0, q0, r0, m0, 'floor-5/temperature', ... (2 bytes))
    15
    Sending PINGREQ
    Received PINGRESP

需要注意的是mosquitto客户端默认使用QoS 0，下面我们使用QoS 2订阅这个主题：
::

    mosquitto_sub -d -q 2 -t 'floor-5/temperature'
    Received CONNACK
    Received SUBACK
    Subscribed (mid: 1): 2

切换到另外SSH连接然后在这个主题里面发送温度消息：
::

    mosquitto_pub -d -q 2 -t 'floor-5/temperature' -m '15'
    Received CONNACK
    Sending PUBLISH (d0, q2, r0, m1, 'floor-5/temperature', ... (2 bytes))
    Received PUBREC (Mid: 1)
    Sending PUBREL (Mid: 1)
    Received PUBCOMP (Mid: 1)

此时回到第一个SSH客户端可以看到信息已经接收到了，以及相应的多次握手消息：
::

    Received PUBLISH (d0, q2, r0, m1, 'floor-5/temperature', ... (2 bytes))
    Sending PUBREC (Mid: 1)
    Received PUBREL (Mid: 1)
    15
    Sending PUBCOMP (Mid: 1)

